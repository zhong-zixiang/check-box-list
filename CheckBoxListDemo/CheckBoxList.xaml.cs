﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheckBoxListDemo
{
    /// <summary>
    /// CheckBoxList.xaml 的交互逻辑
    /// </summary>
    public partial class CheckBoxList : UserControl
    {
        public CheckBoxList()
        {
            InitializeComponent();
        }



        #region 添加修改标题
        /// <summary>
        /// 标题
        /// </summary>
        public string ListTilte
        {
            get { return (string)GetValue(ListTilteProperty); }
            set { SetValue(ListTilteProperty, value); }
        }

        public static readonly DependencyProperty ListTilteProperty =
            DependencyProperty.Register("ListTilte", typeof(string), typeof(CheckBoxList));
        private string _oldTilter = "";
        private void TilteTbx_LostFocus(object sender, RoutedEventArgs e)
        {
            this.OkBtn.Visibility = Visibility.Collapsed;
            this.CancelBtn.Visibility = Visibility.Collapsed;
        }

        private void TilteTbx_GotFocus(object sender, RoutedEventArgs e)
        {
            _oldTilter = ListTilte;
            this.OkBtn.Visibility = Visibility.Visible;
            this.CancelBtn.Visibility = Visibility.Visible;
        }

        private void OkBtn_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _oldTilter = "";
        }

        private void CancelBtn_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ListTilte = _oldTilter;
        }
        #endregion

        #region 添加选择项
        private void submitButton_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(contentTextBox.Text))
            {
                AddItems();
            }
            else
            {
                Keyboard.Focus(contentTextBox);
                e.Handled = true;
            }
        }
        private void CheckBoxCancelButton_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AddBtn.Visibility = Visibility.Collapsed;
            SubmitGrid.Visibility = Visibility.Visible;
            Keyboard.Focus(contentTextBox);
        }
        private void contentTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            AddBtn.Visibility = Visibility.Visible;
            SubmitGrid.Visibility = Visibility.Collapsed;
        }
        /// <summary>
        /// 添加复选框
        /// </summary>
        private void AddItems()
        {
            checkBoxItemsControl.Items.Add(new CheckBox
            {
                Content = contentTextBox.Text,
                IsChecked = false,
                Style = Resources["BlueCheckBox"] as Style,
                Margin = new Thickness(0, 3, 0, 3)
            });
            (checkBoxItemsControl.Items.OfType<CheckBox>()).ToList().Last().Checked += CheckBox_CheckedChanged;
            (checkBoxItemsControl.Items.OfType<CheckBox>()).ToList().Last().Unchecked += CheckBox_CheckedChanged;
            UpDateProgress();
            contentTextBox.Text = string.Empty;
        }

        #endregion

        #region 更新进度条
        private void CheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            var isChecked = (sender as CheckBox)?.IsChecked;
            if (isChecked != null)
            {
                UpDateProgress();
            }
        }
        /// <summary>
        /// 更新进度条
        /// </summary>
        public void UpDateProgress()
        {
            progressBar.Maximum = 100;
            double value = Math.Round(((double)(checkBoxItemsControl.Items.OfType<CheckBox>()).ToList().FindAll(i => i.IsChecked == true).Count / (double)checkBoxItemsControl.Items.Count) * 100, 0);
            progressBar.Value = value;
        }

        #endregion
        /// <summary>
        /// 输入时点击键盘
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void contentTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrWhiteSpace(contentTextBox.Text))
                {
                    AddItems();
                }
                else
                {
                    Keyboard.Focus(contentTextBox);
                    e.Handled = true;
                }
            }
        }
    }
}
